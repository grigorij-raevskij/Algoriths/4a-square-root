﻿#include <iostream>

int SQRT(uint16_t num)
{
    uint16_t result = 0;
    uint16_t start = 0, end = num, middle;
    while (start <= end)
    {
        middle = (start + end) / 2;
        if (middle * middle == num)
            return middle;
        
        if (middle * middle < num)
        {
            start = middle + 1;
            result = middle;
        }
        else
            end = middle - 1;

    }
    return result;
}

int main()
{
    uint16_t x;
    while (std::cin >> x)
        std::cout << SQRT(x) << std::endl;
}
